import ballerina/io;
import ballerina/http;

type body record {
    
};

public type Studententry  record {|
   readonly int student_number;
     string  name ; 
     string   email_address ; 
      string student_courses ;

    
    
|};
public   type Courses record {|

        readonly int course_code ; 
        int weights_assessments;
        int marks_awardedAssessment
;|};
public type student_numberConflict record {|
    *http:Conflict;
    ErrorMsg body;
|};

 public type student_numberNotFound record {|
    *http:NotFound;
    ErrorMsg body;
|};

public  type ErrorMsg record {|
    string errormsg;
|};

table<Studententry> key(student_number) studentsTable = table[];

 type StudentEntries record {|
    *http:Created;
     courses;
     Studententry[] body;
|};

      

public final table<Studententry> key(student_number) studentTable = table [
    { student_number: 1234, name: "Thomas", email_address: "thmas@gmail.com", student_courses :"ENGLISH" },

    { student_number: 1235, name: "Makuban", email_address: "makuban@gmail.com", student_courses :"GEOGRAPHY" }
];

public final table<Courses> key(course_code) course_Table = table [
    {course_code:234,weights_assessments:2,marks_awardedAssessment:50},

    {course_code:235,weights_assessments:2,marks_awardedAssessment:60}

];



service /Studententry/courses on new http:Listener(9000) {


  resource function get all_Student() returns Studententry[] {
        return studentTable.toArray();
  }
  resource function get single_Student () returns Studententry {
    io:println studentTableerror (student_number)=(1234) ;
  }
  resource function post newstudent(@http:Payload Studententry[] StudentEntries)
                                    returns Studententry|Studententry {

    string[] conflictingStudent_number = from Studententry studentEntry in StudentEntries
        where studentTable.hasKey(studentEntry.student_number)
        select Studententry.(name,email_address,student_Courses);

    if conflictingStudent_number.length() > 0 {
        return <StudentEntries>{
            body: {
                errmsg: string:'join("Student_number:", ...conflictingStudent_number)
            }
        };
    } else {
        StudentEntries.forEach(studentEntry => studentTable.add(studentEntry));
        return <Studententry>{body: StudentEntries,name: "Jhone",email_address: "jhone@gmail.com",student_courses: "develpment_studies",student_number: 1};
    }
}


resource function put updateStudent_details/[int studentNum](@http:Payload map<string> Studententry){
        foreach   Studententry i in studentTable{
            if i.student_number == student_number{
                i.Studententry = Studententry;
            }
        }
    }

    resource function put updateCourse_details/[int studentNum](@http:Payload map<string> courses){
        foreach Courses i in course_Table{
            if i.course_code == course_number {
                i.courses  = Courses;
            }
        }
    }

      resource function delete StudentDetails/[int studentNumber](){
    foreach Studententry item in studentTable {
        if item.student_number == studentNumber{
            Studententry remove = studentTable.remove(item.student_number);
            io:print(remove);
        }
    }
    }


  }