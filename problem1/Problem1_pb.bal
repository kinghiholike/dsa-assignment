import ballerina/grpc;

public isolated client class serverClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_PROBLEM1, getDescriptorMapProblem1());
    }

    isolated remote function assign_courses(asignCreatedCourse|ContextAsignCreatedCourse req) returns created_courses|grpc:Error {
        map<string|string[]> headers = {};
        asignCreatedCourse message;
        if req is ContextAsignCreatedCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AGN.server/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <created_courses>result;
    }

    isolated remote function assign_coursesContext(asignCreatedCourse|ContextAsignCreatedCourse req) returns ContextCreated_courses|grpc:Error {
        map<string|string[]> headers = {};
        asignCreatedCourse message;
        if req is ContextAsignCreatedCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AGN.server/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <created_courses>result, headers: respHeaders};
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AGN.server/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AGN.server/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }

    isolated remote function request_assignments() returns Request_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AGN.server/request_assignments");
        return new Request_assignmentsStreamingClient(sClient);
    }

    isolated remote function submit_marks() returns Submit_marksStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AGN.server/submit_marks");
        return new Submit_marksStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AGN.server/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("AGN.server/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendNew_user(new_user message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextNew_user(ContextNew_user message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCreated_user() returns created_user|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <created_user>payload;
        }
    }

    isolated remote function receiveContextCreated_user() returns ContextCreated_user|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <created_user>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendSubmit_assignment(submit_assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextSubmit_assignment(ContextSubmit_assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveSubmited_assignments() returns submited_assignments|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <submited_assignments>payload;
        }
    }

    isolated remote function receiveContextSubmited_assignments() returns ContextSubmited_assignments|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <submited_assignments>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Request_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendRequest_submitedAssignment(request_submitedAssignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextRequest_submitedAssignment(ContextRequest_submitedAssignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveSubmited_assignments() returns submited_assignments|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <submited_assignments>payload;
        }
    }

    isolated remote function receiveContextSubmited_assignments() returns ContextSubmited_assignments|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <submited_assignments>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_marksStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendMarks(marks message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextMarks(ContextMarks message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveSubmited_marks() returns submited_marks|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <submited_marks>payload;
        }
    }

    isolated remote function receiveContextSubmited_marks() returns ContextSubmited_marks|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <submited_marks>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendRegister_course(register_course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextRegister_course(ContextRegister_course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveRegisterd_course() returns registerd_course|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <registerd_course>payload;
        }
    }

    isolated remote function receiveContextRegisterd_course() returns ContextRegisterd_course|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <registerd_course>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendNew_courses(new_courses message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextNew_courses(ContextNew_courses message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCourse_code() returns course_code|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <course_code>payload;
        }
    }

    isolated remote function receiveContextCourse_code() returns ContextCourse_code|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <course_code>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class ServerRegisterdcourseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendRegisterd_course(registerd_course response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextRegisterd_course(ContextRegisterd_course response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ServerSubmitedmarksCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSubmited_marks(submited_marks response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSubmited_marks(ContextSubmited_marks response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ServerCoursecodeCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCourse_code(course_code response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCourse_code(ContextCourse_code response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ServerCreatedcoursesCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreated_courses(created_courses response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreated_courses(ContextCreated_courses response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ServerCreateduserCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreated_user(created_user response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreated_user(ContextCreated_user response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ServerSubmitedassignmentsCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSubmited_assignments(submited_assignments response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSubmited_assignments(ContextSubmited_assignments response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextMarksStream record {|
    stream<marks, error?> content;
    map<string|string[]> headers;
|};

public type ContextNew_coursesStream record {|
    stream<new_courses, error?> content;
    map<string|string[]> headers;
|};

public type ContextRegister_courseStream record {|
    stream<register_course, error?> content;
    map<string|string[]> headers;
|};

public type ContextNew_userStream record {|
    stream<new_user, error?> content;
    map<string|string[]> headers;
|};

public type ContextRequest_submitedAssignmentStream record {|
    stream<request_submitedAssignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourse_codeStream record {|
    stream<course_code, error?> content;
    map<string|string[]> headers;
|};

public type ContextSubmit_assignmentStream record {|
    stream<submit_assignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextSubmited_assignments record {|
    submited_assignments content;
    map<string|string[]> headers;
|};

public type ContextCreated_user record {|
    created_user content;
    map<string|string[]> headers;
|};

public type ContextAsignCreatedCourse record {|
    asignCreatedCourse content;
    map<string|string[]> headers;
|};

public type ContextSubmited_marks record {|
    submited_marks content;
    map<string|string[]> headers;
|};

public type ContextMarks record {|
    marks content;
    map<string|string[]> headers;
|};

public type ContextNew_courses record {|
    new_courses content;
    map<string|string[]> headers;
|};

public type ContextRegisterd_course record {|
    registerd_course content;
    map<string|string[]> headers;
|};

public type ContextRegister_course record {|
    register_course content;
    map<string|string[]> headers;
|};

public type ContextNew_user record {|
    new_user content;
    map<string|string[]> headers;
|};

public type ContextRequest_submitedAssignment record {|
    request_submitedAssignment content;
    map<string|string[]> headers;
|};

public type ContextCourse_code record {|
    course_code content;
    map<string|string[]> headers;
|};

public type ContextSubmit_assignment record {|
    submit_assignment content;
    map<string|string[]> headers;
|};

public type ContextCreated_courses record {|
    created_courses content;
    map<string|string[]> headers;
|};

public type submited_assignments record {|
    int[] assignment_ID = [];
|};

public type created_user record {|
    string[] name = [];
|};

public type asignCreatedCourse record {|
    int[] course_ID = [];
    string[] assessor = [];
|};

public type submited_marks record {|
    int[] weight = [];
|};

public type marks record {|
    int[] assignment_Marks = [];
|};

public type new_courses record {|
    string[] course = [];
    int[] course_ID = [];
|};

public type new_courses_weight record {|
    int[] assignment_ID = [];
    int[] assignment_WEIGHT = [];
|};

public type registerd_course record {|
    int[] code = [];
|};

public type register_course record {|
    int[] course_ID = [];
|};

public type new_user record {|
    string[] name = [];
    int[] user_ID = [];
|};

public type request_submitedAssignment record {|
    string[] assignment_ID = [];
|};

public type course_code record {|
    int[] code = [];
|};

public type registred_course record {|
    int[] course_code = [];
|};

public type submit_assignment record {|
    int[] code = [];
    string[] course_Name = [];
|};

public type created_courses record {|
    int[] code = [];
|};

const string ROOT_DESCRIPTOR_PROBLEM1 = "0A0E50726F626C656D312E70726F746F120341474E22260A107265676973746572645F636F7572736512120A04636F64651801200328055204636F646522210A0B636F757273655F636F646512120A04636F64651801200328055204636F646522250A0F637265617465645F636F757273657312120A04636F64651801200328055204636F646522220A0C637265617465645F7573657212120A046E616D6518012003280952046E616D65223B0A147375626D697465645F61737369676E6D656E747312230A0D61737369676E6D656E745F4944180120032805520C61737369676E6D656E74494422280A0E7375626D697465645F6D61726B7312160A06776569676874180120032805520677656967687422330A107265676973747265645F636F75727365121F0A0B636F757273655F636F6465180120032805520A636F75727365436F6465229E010A0B6E65775F636F757273657312160A06636F757273651801200328095206636F75727365121B0A09636F757273655F49441802200328055208636F7572736549441A5A0A0677656967687412230A0D61737369676E6D656E745F4944180120032805520C61737369676E6D656E744944122B0A1161737369676E6D656E745F574549474854180220032805521061737369676E6D656E74574549474854224D0A12617369676E43726561746564436F75727365121B0A09636F757273655F49441801200328055208636F757273654944121A0A086173736573736F7218022003280952086173736573736F7222370A086E65775F7573657212120A046E616D6518012003280952046E616D6512170A07757365725F4944180220032805520675736572494422480A117375626D69745F61737369676E6D656E7412120A04636F64651801200328055204636F6465121F0A0B636F757273655F4E616D65180220032809520A636F757273654E616D6522410A1A726571756573745F7375626D6974656441737369676E6D656E7412230A0D61737369676E6D656E745F4944180120032809520C61737369676E6D656E74494422320A056D61726B7312290A1061737369676E6D656E745F4D61726B73180120032805520F61737369676E6D656E744D61726B73222E0A0F72656769737465725F636F75727365121B0A09636F757273655F49441801200328055208636F75727365494432C5030A0673657276657212380A0E6372656174655F636F757273657312102E41474E2E6E65775F636F75727365731A102E41474E2E636F757273655F636F646528013001123F0A0E61737369676E5F636F757273657312172E41474E2E617369676E43726561746564436F757273651A142E41474E2E637265617465645F636F757273657312320A0C6372656174655F7573657273120D2E41474E2E6E65775F757365721A112E41474E2E637265617465645F75736572280112490A127375626D69745F61737369676E6D656E747312162E41474E2E7375626D69745F61737369676E6D656E741A192E41474E2E7375626D697465645F61737369676E6D656E7473280112530A13726571756573745F61737369676E6D656E7473121F2E41474E2E726571756573745F7375626D6974656441737369676E6D656E741A192E41474E2E7375626D697465645F61737369676E6D656E7473280112310A0C7375626D69745F6D61726B73120A2E41474E2E6D61726B731A132E41474E2E7375626D697465645F6D61726B73280112390A08726567697374657212142E41474E2E72656769737465725F636F757273651A152E41474E2E7265676973746572645F636F757273652801620670726F746F33";

public isolated function getDescriptorMapProblem1() returns map<string> {
    return {"Problem1.proto": "0A0E50726F626C656D312E70726F746F120341474E22260A107265676973746572645F636F7572736512120A04636F64651801200328055204636F646522210A0B636F757273655F636F646512120A04636F64651801200328055204636F646522250A0F637265617465645F636F757273657312120A04636F64651801200328055204636F646522220A0C637265617465645F7573657212120A046E616D6518012003280952046E616D65223B0A147375626D697465645F61737369676E6D656E747312230A0D61737369676E6D656E745F4944180120032805520C61737369676E6D656E74494422280A0E7375626D697465645F6D61726B7312160A06776569676874180120032805520677656967687422330A107265676973747265645F636F75727365121F0A0B636F757273655F636F6465180120032805520A636F75727365436F6465229E010A0B6E65775F636F757273657312160A06636F757273651801200328095206636F75727365121B0A09636F757273655F49441802200328055208636F7572736549441A5A0A0677656967687412230A0D61737369676E6D656E745F4944180120032805520C61737369676E6D656E744944122B0A1161737369676E6D656E745F574549474854180220032805521061737369676E6D656E74574549474854224D0A12617369676E43726561746564436F75727365121B0A09636F757273655F49441801200328055208636F757273654944121A0A086173736573736F7218022003280952086173736573736F7222370A086E65775F7573657212120A046E616D6518012003280952046E616D6512170A07757365725F4944180220032805520675736572494422480A117375626D69745F61737369676E6D656E7412120A04636F64651801200328055204636F6465121F0A0B636F757273655F4E616D65180220032809520A636F757273654E616D6522410A1A726571756573745F7375626D6974656441737369676E6D656E7412230A0D61737369676E6D656E745F4944180120032809520C61737369676E6D656E74494422320A056D61726B7312290A1061737369676E6D656E745F4D61726B73180120032805520F61737369676E6D656E744D61726B73222E0A0F72656769737465725F636F75727365121B0A09636F757273655F49441801200328055208636F75727365494432C5030A0673657276657212380A0E6372656174655F636F757273657312102E41474E2E6E65775F636F75727365731A102E41474E2E636F757273655F636F646528013001123F0A0E61737369676E5F636F757273657312172E41474E2E617369676E43726561746564436F757273651A142E41474E2E637265617465645F636F757273657312320A0C6372656174655F7573657273120D2E41474E2E6E65775F757365721A112E41474E2E637265617465645F75736572280112490A127375626D69745F61737369676E6D656E747312162E41474E2E7375626D69745F61737369676E6D656E741A192E41474E2E7375626D697465645F61737369676E6D656E7473280112530A13726571756573745F61737369676E6D656E7473121F2E41474E2E726571756573745F7375626D6974656441737369676E6D656E741A192E41474E2E7375626D697465645F61737369676E6D656E7473280112310A0C7375626D69745F6D61726B73120A2E41474E2E6D61726B731A132E41474E2E7375626D697465645F6D61726B73280112390A08726567697374657212142E41474E2E72656769737465725F636F757273651A152E41474E2E7265676973746572645F636F757273652801620670726F746F33"};
}

