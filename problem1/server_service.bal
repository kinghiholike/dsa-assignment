import ballerina/grpc;

listener grpc:Listener ep = new (9090);
//data structure
type courses record{
    string name;
}
@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_PROBLEM1, descMap: getDescriptorMapProblem1()}
service "server" on ep {
    
    remote function assign_courses(asignCreatedCourse value) returns created_courses|error {
    }
    remote function create_users(stream<new_user, grpc:Error?> clientStream) returns created_user|error {
    }
    remote function submit_assignments(stream<submit_assignment, grpc:Error?> clientStream) returns submited_assignments|error {
    }
    remote function request_assignments(stream<request_submitedAssignment, grpc:Error?> clientStream) returns submited_assignments|error {
    }
    remote function submit_marks(stream<marks, grpc:Error?> clientStream) returns submited_marks|error {
    }
    remote function register(stream<register_course, grpc:Error?> clientStream) returns registerd_course|error {
    }
    remote function create_courses(stream<new_courses, grpc:Error?> clientStream) returns stream<course_code, error?>|error {
    }
}

